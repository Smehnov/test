import glob
from pathlib import Path

from rosbags.highlevel import AnyReader



def main():
    files = glob.glob('/data/*')
    with AnyReader([Path('/data/state.bag')]) as reader:
        connections = [x for x in reader.connections if x.topic == '/tf']
        for connection, timestamp, rawdata in reader.messages(connections=connections):
            msg = reader.deserialize(rawdata, connection.msgtype)
            print(msg.header.frame_id)


if __name__ == '__main__':
    main()
